# Crosswords Puzzle Set for Niente per niente

This repo contains an Italian puzzle set downloader for [GNOME Crosswords](https://gitlab.gnome.org/jrb/crosswords). The puzzles are pulled from [BOOM!!! niente per niente](https://www.nienteperniente.it/enigmistica.asp).
